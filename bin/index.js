const app = require("../src/app");
const http = require("http");
const debug = require("debug")("nodestr:server");

const port = process.env.PORT || 8080;

app.set("port", port);
const server = http.createServer(app);


server.on("error", onError);
server.on("listening", onListening);
app.listen(process.env.PORT || port, () =>
  console.log(`app listening on port ${port} with Address http://localhost:${port}/`)
);

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string" ? "pipe " + port : "port " + port;

  switch (error.code) {
    case "EACESS":
      console.error(`${bind} requer privilegios elevedos`);
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
      break;
  }
}

function onListening(val) {
    const addr = server.address();
    const bind = typeof port === "string" ? "pipe " + port : "port " + port;
    debug('Listening on' + bind);
}
