"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Customer",
  },
  number: {
    type: String,
    require: [true, "o number é obrigatorio"],
    trim: true,
  },
  createDate: {
    type: Date,
    require: true,
    default: Date.now,
  },
  status: {
    type: String,
    require: true,
    enum: ["created", "done"],
    default: "created",
  },
  password: {
    type: String,
    require: true,
    trim: true,
  },
  items:[{
    quantity: {
        type: Number,
        require: true,
        default : 1
      },
      price: {
          type: String,
          require : true
      },
      product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
      }
  }]
});

module.exports = mongoose.model("Order", schema);
