"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    require: [true, "o name é obrigatorio"],
    trim: true,
  },
  email: {
    type: String,
    require: [true, "o email é obrigatorio"],
    trim: true,
  },
  password: {
    type: String,
    require: [true, "o password é obrigatorio"],
    trim: true,
  },
  roles: [
    {
      type: String,
      require: true,
      enum: ["user", "admin"],
      default: "user",
    },
  ],
});

module.exports = mongoose.model("Customer", schema);
