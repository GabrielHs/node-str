"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  tittle: {
    type: String,
    require: [true, 'o tittle é obrigatorio'],
    trim: true,
  },
  slug: { //cadeira game = cadeira-game
    type: String,
    require: true,
    trim: true,
    index: true,
    unique: true,
  },
  description: {
    type: String,
    require: true,
    trim: true,
  },
  price: {
    type: Number,
    require: true,
  },
  active: {
    type: Boolean,
    require: true,
    default:true
  },
  tags:[{
      type: String,
      require: true
  }],
  image: {
    type: String,
    require: true,
    trim: true,
  }
});


/*
    Example json

    {
        "title" : "titulo",
        "Description" : "xpto",
        tags:[
            "teste", "213", "pessoas"
        ]
    }

*/

module.exports = mongoose.model("Product", schema);
