"use strict";

const mongoose = require("mongoose");
const customerModel = mongoose.model("Customer");

exports.create = async (data) => {
  var model = new customerModel(data);

  await model.save();
};


exports.authenticate = async(data) => {
  let res = await customerModel.findOne({
      email: data.email,
      password: data.password
  });

  return res;
}

exports.getById = async(id) => {
  let res = await customerModel.findById(id);

  return res;
}



//Diferença do find para findOne
//find = retorna um array de objetos
//findOne = retorna um obj ou vazio