"use strict";

const mongoose = require("mongoose");
const productModel = mongoose.model("Product");

exports.get =  () => {
  let res = productModel.find(
    {
      active: true,
    },
    "tittle slug price"
  );
  return res;
};

exports.getBySlug = async (slug) => {
  let res = await productModel.findOne(
    {
      slug: slug,
      active: true,
    },
    "tittle description slug price tags"
  );
  return res;
};

exports.getById = async (id) => {
  let res = await productModel.findById(id);
  return res;
};

exports.getByTag = async (tag) => {
  let res = await  productModel.find(
    {
      tags: tag,
      active: true,
    },
    "tittle description price slug tags"
  );
  return res;
};

exports.create = async (data) => {
  var model = new productModel(data);

  await model.save();
};

exports.edit = async (id, data) => {
  await productModel.findByIdAndUpdate(id, {
    $set: {
      tittle: data.tittle,
      description: data.description,
      price: data.price,
    },
  });
};

exports.DelProduto = async (id) => {
  await productModel.findOneAndRemove(id);
};
