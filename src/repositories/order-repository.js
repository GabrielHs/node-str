"use strict";

const mongoose = require("mongoose");
const orderModel = mongoose.model("Order");

exports.get = async (data) => {
  var rest = await orderModel.find({}, 'number customer items status')
  .populate('customer', 'name')
  .populate('items.product', 'tittle');

  return rest;
};

exports.create = async (data) => {
  var model = new orderModel(data);

  await model.save();
};
