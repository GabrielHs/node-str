"use strict";
const ValidationContract = require("../validators/fluent-validator");
const customerRepository = require("../repositories/customer-repository");
const md5 = require("md5");
const emailService = require("../services/email-service");
const fs = require("fs");
const path = require("path");
const helper = require("../utils/helper");
const authService = require("../services/auth-service");

exports.cadastro = async (req, res, next) => {
  let contract = new ValidationContract();

  contract.hasMinLen(
    req.body.name,
    3,
    "O nome deve conter pelo menos 3 caracteres"
  );
  contract.isEmail(req.body.email, "Email inválido");
  contract.hasMinLen(
    req.body.password,
    6,
    "A senha deve conter pelo menos 6 caracteres"
  );

  //Se os dados forem inválidos
  if (!contract.isValid()) {
    res.status(400).send(contract.errors()).end();
    return;
  }

  let dataRequest = helper.RetornaDataHoraAtual;

  let bodyEmail;
  fs.readFile(
    path.join(__dirname, "../template/", `bem-vindo.html`),
    "utf8",
    (err, data) => {
      if (err) throw err;
      bodyEmail = data
        .replace("{nome_cliente}", req.body.name)
        .replace("{data_request}", dataRequest);
      //console.log(data.replace('{dados}', req.body.name));
    }
  );

  try {
    await customerRepository.create({
      name: req.body.name,
      email: req.body.email,
      password: md5(req.body.password + global.SALT_KEY),
      roles:["user"]
    });

    //Envio do email
    emailService.envioEmail(
      req.body.email,
      "Bem vindo ao Node Store",
      bodyEmail
      // global.EMAIL_TMPL.replace("{0}", req.body.name)
    );

    res.status(201).send({
      ok: {
        msg: "Cliente cadastrado com sucesso",
      },
    });
  } catch (error) {
    res.status(400).send({
      erro: {
        msg: "Falha na requisição",
        data: error,
      },
    });
  }
};

exports.authenticateLogin = async (req, res, next) => {
  try {
    const customer = await customerRepository.authenticate({
      email: req.body.email,
      password: md5(req.body.password + global.SALT_KEY),
    });

    if (!customer) {
      res.status(404).send({
        msg: "User ou senha inválido",
      });
      return;
    }
    const tokenLogin = await authService.generateToken({
      id: customer._id,
      email: customer.email,
      name: customer.name,
      roles: customer.roles
    });

    res.status(201).send({
      ok: {
        token: tokenLogin,
        data: {
          email: customer.email,
          name: customer.name,
          roles: customer.roles
        },
      },
    });
  } catch (error) {
    res.status(400).send({
      erro: {
        msg: "Falha na requisição",
        data: error,
      },
    });
  }
};

exports.refreshToken = async (req, res, next) => {
  try {
    //recupera o token
    let token =
      req.body.token || req.query.token || req.headers["x-acesss-token"];

    let dataDecode = await authService.decodetoken(token);

    const customer = await customerRepository.getById(dataDecode.id);

    if (!customer) {
      res.status(404).send({
        msg: "Cliente não encontrado",
      });
      return;
    }
    const tokenLogin = await authService.generateToken({
      id: customer._id,
      email: customer.email,
      name: customer.name,
      roles: customer.roles

    });

    res.status(201).send({
      ok: {
        token: tokenLogin,
        data: {
          email: customer.email,
          name: customer.name,
          
        },
      },
    });
  } catch (error) {
    res.status(400).send({
      erro: {
        msg: "Falha na requisição",
        data: error,
      },
    });
  }
};
