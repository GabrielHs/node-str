"use strict";

//const mongoose = require("mongoose");

//const productModel = mongoose.model("Product");

const ValidationContract = require("../validators/fluent-validator");

const productRepository = require("../repositories/product-repository");

const helper = require('../utils/helper');

/*
 Actions dos controller
 */

//Action para listar
exports.listProducts = (req, res, next) => {
  // try {
  //   var data = await productRepository.get();
  //   res.status(200).send({
  //     ok: data,
  //   });
  // } catch (error) {
  //   res.status(500).send({
  //     erro: {
  //       msg: "Erro ao listar os produtos",
  //       data: error,
  //     },
  //   });
  // }

  productRepository
    .get()
    .then((data) => {
      res.status(200).send({
        ok: data,
      });
    })
    .catch((e) => {
      res.status(500).send({
        erro: {
          msg: "Erro ao listar os produtos",
          data: e,
        },
      });
    });
};

exports.getBySlug = async (req, res, next) => {
  try {
    let idSlug = req.params.slug;

    var data = await productRepository.getBySlug(idSlug);
    res.status(200).send({
      ok: data,
    });
  } catch (error) {
    res.status(500).send({
      erro: {
        msg: "Erro ao exibir produto pelo slug",
        data: error,
      },
    });
  }
};

exports.getById = async (req, res, next) => {
  try {
    let id = req.params.id;
    var data = await productRepository.getById(id);

    res.status(200).send({
      ok: data,
    });
  } catch (error) {
    res.status(500).send({
      erro: {
        msg: "Erro ao listar os produtos",
        data: error,
      },
    });
  }
};

exports.getByTag = async (req, res, next) => {
  try {
    var data = await productRepository.getByTag(req.params.tag);

    res.status(200).send({
      ok: data,
    });
  } catch (error) {
    res.status(500).send({
      erro: {
        msg: "Erro ao listar os produtos",
        data: error,
      },
    });
  }
};

exports.cadastro = async (req, res, next) => {
  //var model = new productModel();
  // model.tittle = req.body.tittle; obs:[Essa forma é idela para tratar cada item, porem como é para estudo usado da forma abaixo]
  //  var model = new productModel(req.body);

  let contract = new ValidationContract();

  contract.hasMinLen(
    req.body.tittle,
    3,
    "O titulo deve conter pelo menos 3 caracteres"
  );
  contract.hasMinLen(
    req.body.slug,
    3,
    "O slug deve conter pelo menos 3 caracteres"
  );
  contract.hasMinLen(
    req.body.description,
    3,
    "O description deve conter pelo menos 3 caracteres"
  );

  //Se os dados forem inválidos
  if (!contract.isValid()) {
    res.status(400).send(contract.errors()).end();
    return;
  }

  //var base64str = helper.base64_encode(req.body.image);

  try {
    await productRepository.create({
      tittle: req.body.tittle,
      slug: req.body.slug,
      description: req.body.description,
      active: req.body.active,
      tags: req.body.tags,
      image: req.body.image
    });
    res.status(201).send({
      ok: {
        msg: "Produto cadastrado com sucesso",
      },
    });
  } catch (error) {
    res.status(400).send({
      erro: {
        msg: "Erro ao cadastrar produto",
        data: error,
      },
    });
  }
};

exports.put = async (req, res, next) => {
  try {
    let idProduct = req.params.id;
    await productRepository.edit(idProduct, req.body);

    res.status(200).send({
      ok: {
        message: `Produto atualizado com sucesso`,
      },
    });
  } catch (error) {
    res.status(400).send({
      error: {
        message: "Falha ao atualizar produto",
        data: error,
      },
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let idProduct = req.params.id;
    await productRepository.DelProduto(idProduct);
    res.status(200).send({
      ok: {
        message: `Produto deletado com sucesso`,
      },
    });
  } catch (error) {
    res.status(400).send({
      error: {
        message: "Falha ao deletar produto",
        data: error,
      },
    });
  }
};
