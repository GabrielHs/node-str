"use strict";
const orderRepository = require("../repositories/order-repository");
const guid = require("guid");
const authService = require('../services/auth-service');


exports.get = async (req, res, next) => {
  try {
    var data = await orderRepository.get();
    res.status(200).send({
      ok: data,
    });
  } catch (error) {
    res.status(500).send({
      erro: {
        msg: "Erro ao listar os produtos",
        data: error,
      },
    });
  }
};

exports.cadastro = async (req, res, next) => {
  try {
    //recupera o token
    let token = req.body.token || req.query.token || req.headers["x-acesss-token"];

    let dataDecode = await authService.decodetoken(token);


    await orderRepository.create({
      customer: dataDecode.id,
      number: guid.raw().substr(0, 6),
      items: req.body.items,
    });
    res.status(201).send({
      ok: {
        msg: "Pedido cadastrado com sucesso",
      },
    });
  } catch (error) {
    res.status(400).send({
      erro: {
        msg: "Falha na requisição",
        data: error,
      },
    });
  }
};
