"use strinct";

//import modules
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const config = require("./config");

//const { connection_string_mongo } = require('../config.json');

// mongoose.connect(connection_string_mongo,{useNewUrlParser: true,  useUnifiedTopology: true, useFindAndModify: false});

//Conexao com banco
// 1°
mongoose.set("useCreateIndex", true);
mongoose.connect(config.connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

//3°
// mongoose.connect(connection_string_mongo, {
//     useUnifiedTopology: true,useNewUrlParser: true,
// })
// .then(() => console.log('DB Connected!'))
// .catch(err => {
// console.error(`DB Connection Error: ${err.message}`);
// });

//Carrega os models
const productModel = require("./models/produtcModel");
const customeModel = require("./models/customerModel");
const orderModel = require("./models/orderModel");

//Carrega rotas
const defaultRoute = require("./routes/default-route");
const productsRoute = require("./routes/product-route");
const customerRoute = require("./routes/customer-route");
const orderRoute = require("./routes/order-route");

//pre-config modules
//app.use(bodyParser.json());

//setar limite para requisições em json
app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    extended: false
}));

// Habilita o CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});





const router = express.Router();

//Rotas e route-prefix
app.use("/", defaultRoute);
app.use("/api", productsRoute);
app.use("/api", customerRoute);
app.use("/orders", orderRoute);

//Exportação do app para ser usando em outros arquivos com importação
module.exports = app;
