"use strict";

const jwt = require("jsonwebtoken");

exports.generateToken = async (data) => {
  return jwt.sign(data, global.SALT_KEY, { expiresIn: "1d" });
};

exports.decodetoken = async (token) => {
  let data = await jwt.verify(token, global.SALT_KEY);
  return data;
};

exports.authorize = function (req, res, next) {
  let token =
    req.body.token || req.query.token || req.headers["x-acesss-token"];

  if (!token) {
    res.status(401).json({
      msg: "Acesso restrito",
    });
  } else {
    jwt.verify(token, global.SALT_KEY, function (error, decoded) {
      if (error) {
        res.status(401).json({
          msg: "Token inválido",
        });
      } else {
        next();
      }
    });
  }
};


exports.isAdmin = function (req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (!token) {
      res.status(401).json({
          message: 'Acesso restrito'
      });
  } else {
      jwt.verify(token, global.SALT_KEY, function (error, decoded) {
          if (error) {
              res.status(401).json({
                  message: 'Token Inválido'
              });
          } else {
              if (decoded.roles.includes('admin')) {
                //includes = verifica o valor dentro de um array
                  next();
              } else {
                  res.status(403).json({
                      message: 'Esta funcionalidade é restrita para administradores'
                  });
              }
          }
      });
  }
};