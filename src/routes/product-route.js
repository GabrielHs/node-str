"use strict";
const express = require("express");
const router = express.Router();
const controllerProduct = require("../controllers/product-controller");
const authService = require('../services/auth-service');


router.get("/products/lista", controllerProduct.listProducts);

router.get("/products/slug/:slug", controllerProduct.getBySlug);

router.get("/products/id/:id", controllerProduct.getById);

router.get("/products/tag/:tag", controllerProduct.getByTag);

router.post("/products", authService.isAdmin, controllerProduct.cadastro);

router.put("/products/:id", authService.isAdmin, controllerProduct.put);

router.delete("/products/:id", authService.isAdmin, controllerProduct.delete);



module.exports = router;
