"use strict";
const express = require("express");
const router = express.Router();
const controllerCustomer = require("../controllers/customer-controller");
const authService = require('../services/auth-service');



router.post("/customers", controllerCustomer.cadastro);

router.post("/customers/login", controllerCustomer.authenticateLogin);

router.post("/customers/refresh-token", authService.authorize, controllerCustomer.refreshToken);




module.exports = router;