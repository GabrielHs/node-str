"use strict";
const express = require("express");
const router = express.Router();
const controllerOrder = require("../controllers/order-controller");
const authService = require('../services/auth-service');



router.get("/", authService.authorize, controllerOrder.get);

router.post("/", authService.authorize, controllerOrder.cadastro);





module.exports = router;